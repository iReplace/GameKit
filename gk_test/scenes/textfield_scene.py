import pygame
import game_kit as gk

class TextFieldScene(gk.Scene):
    def __init__(self, container):
        super().__init__(container)

        self.register(gk.TextField({
            'name': 'TextField',
            'size': gk.SIZE_COEF,
            'width': 1/2,
            'height': 1/20,
            'position': gk.POSITION_COEF,
            'x': 1/4,
            'y': 9/20,
            'font': pygame.font.Font(None, 30),
            'text': 'Text',
            'text_align': gk.TEXT_CENTER,
            'text_color': (0, 0, 0),
            'text_color_active': (255, 255, 255),
            'fill': (255, 255, 255),
            'fill_active': (255, 0, 0),
            'cursor_color': (0, 0, 0),
            'callback': None,
            'keep_active': False,
            'clickable': True
        }, self.container))