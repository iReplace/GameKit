

class Entity:

    def __init__(self, container):
        self.container = container
        self.ev_manager = self.container.ev_manager
        # On s'enregistre auprès de l'EventManager
        self.ev_manager.register(self)

        self._active = False

        from weakref import WeakKeyDictionary
        self.items = WeakKeyDictionary()

    def register(self, item, layer=1):
        self.items[item] = layer

    def unregister(self, item):
        if item in self.items.keys():
            del self.items[item]

    def update(self, dt):
        # On transmet l'update à tous les items dans l'ordre
        for item in self.items.keys():
            item.update(dt)

    def draw(self, surface):
        # On transmet la surface à tous les items pour qu'ils se dessinent
        for item in sorted(self.items, key=self.items.get):
            if hasattr(item, 'draw'):
                item.draw(surface)

    def handle(self, event):
        # Si l'élément n'est pas actif
        if not self._active:
            return