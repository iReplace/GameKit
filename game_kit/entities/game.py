from .entity import *


class Game(Entity):

    def __init__(self, container):
        super().__init__(container)

        self.title = '[Game Title]'
        self.window_size = (1280, 720)

    def update(self, dt):
        # On transmet l'update à tous les items dans l'ordre
        for item in self.items.keys():
            item.update(dt)