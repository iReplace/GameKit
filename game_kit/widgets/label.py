from .widget import *


class Label(Widget):
    def __init__(self, structure, container):
        super().__init__(structure, container)

        # Valeur par défaut
        if not 'clickable' in self:
            self['clickable'] = False

        self.clear()

    def flash(self):

        text_color = self['text_color']
        if self._active and 'text_color_active' in self:
            text_color = self['text_color_active']

        # Génération du texte
        text_view = self['font'].render(self['text'], 1, text_color)
        posXY = self.rect.x, self.rect.y
        # On centre par défaut le texte
        offset_x = self.rect.w/2 - text_view.get_width()/2
        offset_y = self.rect.h/2 - text_view.get_height()/2
        # On modifie la position en fonction de l'alignement du texte
        if self['text_align'] == TEXT_LEFT:
            offset_x = int(self.rect.w * TEXTVIEW_MIN_INTERVAL)
        elif self['text_align'] == TEXT_RIGHT:
            offset_x = self.rect.w * (1 - TEXTVIEW_MIN_INTERVAL) - text_view.get_width()

        # On garde l'offset du au décallage du texte en mémoire (nouvelle position -  ancienne position)
        self.offset = offset_x, offset_y

        # On place la vue créée en mémoire
        self.surface = text_view


"""
    Example :

    Label({
        'name': 'Label',
        'size': SIZE_ABSOLUTE|SIZE_COEF,
        'width': 1,
        'height': 1,
        'position': POSITION_ABSOLUTE|POSITION_COEF,
        'x': 0,
        'y': 0,
        'font': pygame.font.Font(None, 30),
        'text': 'Text',
        'text_align': TEXT_CENTER|TEXT_LEFT|TEXT_RIGHT,
        'text_color': (255, 255, 255)
        'text_color_active': (255, 255, 255)
    }, ev_manager)
"""