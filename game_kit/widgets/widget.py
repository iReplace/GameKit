import pygame
from ..constants import *
# Permet de récupérer les évènements associés au matériel
from ..controllers.device_controllers import *


class Widget:

    def __repr__(self):
        return self['name']

    def __init__(self, structure, container):
        # La structure ne doit pas être altérer par l'implémentation du widget
        self.structure = structure
        # On définit si l'élément est actif ou non
        self._active = self.get('default_active', False)
        self.container = container
        self.ev_manager = self.container.ev_manager
        # On s'enregistre dans la liste de distribution de l'EventManager
        self.ev_manager.register(self)

        # On initialise les valeurs par défaut
        # Le rect correspond au placement du widget
        self.rect = pygame.Rect(0, 0, 0, 0)
        # L'offset permet de garder en mémoire un décallage
        self.offset = (0, 0)
        # Offset donné par le parent qui demande le dessin
        self.givenOffset = (0, 0)
        # La surface stocke le dessin du Widget
        self.surface = pygame.Surface(self.rect.size)
        self.surface_size = self.rect.size

        # Attribut qui permet au parent de savoir si il a besoin de se redessiner
        self.request_parent_clear = False

        # Pour savoir si l'item a déjà été dessiné
        self.first_draw = True

    def __del__(self):
        # On s'enlève de la liste de distribution de l'EventManager à la destruction
        self.ev_manager.unregister(self)

    # Raccourci d'acces à la structure
    def __getitem__(self, item):
        return self.structure[item]

    # Raccourci de modification de la structure
    def __setitem__(self, key, value):
        self.structure[key] = value

    # Vérification rapide de la présence d'un élément
    def __contains__(self, item):
        if item in self.structure:
            return True
        return False

    # Permet de récupérer un élement avec une valeur par défaut si il n'existe pas
    def get(self, item, default):
        return self.structure.get(item, default)

    def set_active(self, active):
        # Si l'état est différent est que le nouvelle état est "actif" on emet un évènement
        if self._active != active and active:
            self.ev_manager.post(BecomeActiveEvent(self))
        self._active = active
        # On nettoie l'ancienne vu
        self.clear()

    def isActive(self):
        return self._active

    def compute_size(self, rect):
        # On prend le rectangle en argument pour les calculs à l'aide des coefficients
        # On place des valeurs par défaut
        self.rect.size = 0, 0
        # On vérfie bien la présence de 'size'
        if 'size' in self:
            if self['size'] == SIZE_ABSOLUTE:
                self.rect.w = self['width']
                self.rect.h = self['height']
            elif self['size'] == SIZE_COEF:
                self.rect.w = rect.w * self['width']
                self.rect.h = rect.h * self['height']

    def compute_position(self, rect):
        # On prend le rectangle en argument pour les calculs à l'aide des coefficients
        self.rect.x, self.rect.y = 0, 0
        # On vérfie bien la présence de 'position'
        if 'position' in self:
            if self['position'] == POSITION_ABSOLUTE:
                self.rect.x = self['x']
                self.rect.y = self['y']
            elif self['position'] == POSITION_COEF:
                self.rect.x = rect.w * self['x']
                self.rect.y = rect.h * self['y']

    def get_actual_position(self):
        return self.rect.x + self.givenOffset[0] + self.offset[0], self.rect.y + self.givenOffset[1] + self.offset[1]

    # Permet de mettre à jour les élements et fournissant le lapse de temps écoulé depuis la dernière frame
    def update(self, dt):
        pass

    # Permet de dessiner sur la surface donnée avec un argument optionel qui correspond au déplacement de l'élément
    # par rapport à sa position original
    def draw(self, surface, offset=(0, 0)):
        # Si la vue n'est pas en mémoire on l'a créer avec flash
        if not self.surface:
            self.flash()

        # On récupère une copie du rectangle de positionnement
        blit_rect = self.rect.copy()
        # On ajuste sa position avec l'offset donné ainsi que l'offset en mémoire
        blit_rect.x += offset[0] + self.offset[0]
        blit_rect.y += offset[1] + self.offset[1]

        # On stocke l'offset donné par le parent qui demande le dessin
        self.givenOffset = offset

        # print(self.offset)
        # On dessine sur la surface donnée
        surface.blit(self.surface, blit_rect)

        # On stocke la taille de la surface
        self.surface_size = self.surface.get_size()

        # Ce n'est plus le premier dessin
        self.first_draw = False

    # Permet de créer une vue flash du widget qui sera stocké en mémoire
    def flash(self):
        self.surface = pygame.Surface(self.rect.size)

    # Supprime la vue actulement stocké en mémoire
    def clear(self, clear_request=True):
        self.request_parent_clear = clear_request
        self.surface = None

    def check_clear_request(self):
        clear_request = self.request_parent_clear
        self.request_parent_clear = False
        return clear_request

    # Vérifie si l'élément collide bien avec les coordonnées données
    def is_click(self, pos):
        hitbox = self.rect.copy()
        hitbox.x += self.givenOffset[0] + self.offset[0]
        hitbox.y += self.givenOffset[1] + self.offset[1]
        return hitbox.collidepoint(pos)

    # Prise en charge par défaut des évènement
    def handle(self, event):

        if isinstance(event, BecomeActiveEvent):
            # Si l'envoyeur est différent du Widget actuel et que ce dernier n'a as de consigne pour rester actif
            # on le rend inactif
            if event.sender != self and not self.get('keep_active', False):
                self.set_active(False)

        # Simple clic (gauche)
        if isinstance(event, MouseLeftDownEvent):
            if self.get('clickable', True):
                # Si le clique est sur l'élément
                if self.is_click(event.pos):
                    # Il devient actif
                    self.set_active(True)
                else:
                    # Sinon il devient inactif
                    self.set_active(False)

        # Double clic (gauche)
        if isinstance(event, MouseDoubleClickEvent):
            if self.is_click(event.pos) and self.get('clickable', True):
                if 'callback' in self and self['callback']:
                    self.ev_manager.post(self['callback'](self))


# EVENTS

class BecomeActiveEvent(Event):
    def __init__(self, sender):
        super().__init__(sender)
        self.name = "Became Active Event"