from .widget import *
import textwrap


class TextBlock(Widget):
    def __init__(self, structure, container):
        super().__init__(structure, container)

        # Stockages des vues des différentes lignes
        self.text_views = None

        # Valeur par défaut
        if not 'clickable' in self:
            self['clickable'] = False

        self.clear()

    def flash(self):

        text_color = self['text_color']
        if self._active and 'text_color_active' in self:
            text_color = self['text_color_active']

        text_size = self['font'].size(self['text'])
        char_av_width = text_size[0]/len(self['text'])
        char_per_line = int(self.rect.w / char_av_width)

        wrapper = textwrap.TextWrapper(width=char_per_line, break_long_words=True, replace_whitespace=True)

        # On commence par repérer les nouvelles lignes
        lines = self['text'].split('\n')
        computed_lines = []

        # Pour chaque ligne on voit si on a besoin de la diviser par rapport à sa longueur
        for index, line in enumerate(lines):
            cropped_lines = wrapper.wrap(line)
            for cropLine in cropped_lines:
                computed_lines.append(cropLine)

        self.text_views = []

        # On récupère la hauteur de ligne ou on en met par défaut la hauteur de la ligne + 4
        lineheight = self.get('lineheight', text_size[1]+4)

        # Pour chaque ligne on effectue un rendu
        for index, line in enumerate(computed_lines):

            # Génération du texte
            text_view = self['font'].render(line, 1, text_color)
            posXY = self.rect.x, self.rect.y
            # On centre par défaut le texte
            offset_x = self.rect.w/2 - text_view.get_width()/2
            offset_y = self.rect.h/2 - text_view.get_height()/2
            # On modifie la position en fonction de l'alignement du texte
            if self['text_align'] == TEXT_LEFT:
                offset_x = int(self.rect.w * TEXTVIEW_MIN_INTERVAL)
            elif self['text_align'] == TEXT_RIGHT:
                offset_x = self.rect.w * (1 - TEXTVIEW_MIN_INTERVAL) - text_view.get_width()

            offset_y = index * lineheight

            self.text_views.append([text_view, (offset_x, offset_y)])

    def draw(self, surface, offset=(0, 0)):
        # Si la vue n'est pas en mémoire on l'a créer avec flash
        if not self.text_views:
            self.flash()

        # On récupère une copie du rectangle de positionnement
        blit_rect = self.rect.copy()
        # On ajuste sa position avec l'offset donné
        blit_rect.x += offset[0]
        blit_rect.y += offset[1]

        for text_view in self.text_views:
            # On créer le rectangle de position de la ligne
            text_view_rect = blit_rect.copy()
            text_view_rect.x += text_view[1][0]
            text_view_rect.y += text_view[1][1]

            # On vérifie si la ligne dépasse de la taille maximum
            if text_view_rect.y + text_view[0].get_height() > self.rect.x + self.rect.h and not self.get('allow_overflow', True):
                continue

            # On dessine sur la surface donnée
            surface.blit(text_view[0], text_view_rect)

        # Ce n'est plus le premier dessin
        self.first_draw = False

    def clear(self, clear_request=True):
        self.text_views = None


"""
    Example :

    TextBlock({
        'name': 'TextBlock',
        'size': SIZE_ABSOLUTE|SIZE_COEF,
        'width': 1,
        'height': 1,
        'position': POSITION_ABSOLUTE|POSITION_COEF,
        'x': 0,
        'y': 0,
        'font': pygame.font.Font(None, 30),
        'text': 'Text',
        'lineheight': 0,
        'allow_overflow': True,
        'text_align': TEXT_CENTER|TEXT_LEFT|TEXT_RIGHT,
        'text_color': (255, 255, 255),
        'text_color_active': (255, 255, 255)
    }, ev_manager)
"""