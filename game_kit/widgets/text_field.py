from .widget import *
from ..constants import *
# Permet de récupérer les évènements associés au matériel
import pygame
from ..controllers.device_controllers import *
from ..utils import *


class TextField(Widget):
    def __init__(self, structure, container):
        super().__init__(structure, container)

        # On initialise la position du curseur à la fin
        self.cursor_pos = len(self['text'])
        self.cursor_rect = pygame.Rect(0, 0, 0, 0)

        # Sélection
        self.select_cursor_pos = 0
        self.selection = False

        # Déplacement du texte pour son alignement (gauche, centre, droite)
        self.text_offset = (0, 0)

        # Timer pour faire clignoter le curseur
        self.cursor_timer = Timer({
            'delay': 500, #ms
            'loop': True,
            'state': [
                True,
                False
            ]
        })

        self.clear()

    def update(self, dt):
        self.cursor_timer.update(dt)

    def flash(self):
        # Sélection de la couleur du texte
        text_color = self['text_color']
        if self._active and 'text_color_active' in self:
            text_color = self['text_color_active']

        # Génération du texte
        text_view = self['font'].render(self['text'], 1, text_color)
        posXY = self.rect.x, self.rect.y
        # On centre par défaut le texte
        offset_x = self.rect.w/2 - text_view.get_width()/2
        offset_y = self.rect.h/2 - text_view.get_height()/2
        # On modifie la position en fonction de l'alignement du texte
        if self['text_align'] == TEXT_LEFT:
            offset_x = int(self.rect.w * TEXTVIEW_MIN_INTERVAL)
        elif self['text_align'] == TEXT_RIGHT:
            offset_x = self.rect.w * (1 - TEXTVIEW_MIN_INTERVAL) - text_view.get_width()

        # On stocke cet offset pour l'alignement du texte notament pour le placement curseur
        self.text_offset = (offset_x, offset_y)

        # On place la vue créée en mémoire
        self.surface = text_view

        # On stocke la taille de la surface
        self.surface_size = self.surface.get_size()

    def append_text(self, text):
        if text != '':
            # On enlève le texte sélectionner
            if self.selection:
                self.remove_selected_text()
            self['text'] = self['text'][:self.cursor_pos] + text + self['text'][self.cursor_pos:]
            self.cursor_pos += len(text)
            self.clear()

    def remove_text(self, dir):
        # Par rapport à la direction dans laquelle on doit supprimer on s'adapte
        if dir == LEFT and self.cursor_pos > 0:
            self['text'] = self['text'][:self.cursor_pos - 1] + self['text'][self.cursor_pos:]
            self.cursor_pos -= 1
        elif dir == RIGHT and self.cursor_pos < len(self['text']):
            self['text'] = self['text'][:self.cursor_pos] + self['text'][self.cursor_pos + 1:]
            # Pas de changement de position de curseur car le texte se déplace vers le curseur
        self.clear()

    def update_text(self, text):
        self['text'] = text
        self.cursor_pos = len(self['text'])
        self.clear()

    def move_cursor(self, dir):
        new_cursor_pos = self.cursor_pos
        if dir == RIGHT:
            new_cursor_pos += 1
            if new_cursor_pos > len(self['text']):
                new_cursor_pos -= 1
        elif dir == LEFT:
            new_cursor_pos -= 1
            if new_cursor_pos < 0:
                new_cursor_pos += 1
        self.cursor_pos = new_cursor_pos

    def select_text(self, dir):
        if not self.selection:
            self.select_cursor_pos = self.cursor_pos
            self.move_cursor(dir)
            self.selection = True
        else:
            self.move_cursor(dir)

    def get_selected_text(self):
        if self.cursor_pos > self.select_cursor_pos:
            return self['text'][self.select_cursor_pos:self.cursor_pos]
        else:
            return self['text'][self.cursor_pos:self.select_cursor_pos]

    def remove_selected_text(self):
        if self.cursor_pos > self.select_cursor_pos:
            self['text'] = self['text'][:self.select_cursor_pos] + self['text'][self.cursor_pos:]
            # replace le curseur à la bonne place
            self.cursor_pos = self.select_cursor_pos
        else:
            self['text'] = self['text'][:self.cursor_pos] + self['text'][self.select_cursor_pos:]

        self.selection = False
        self.clear()


    def handle(self, event):
        # On appelle la méthode originale
        super().handle(event)

        # Si l'élement n'est pas actif on passe
        if not self._active:
            return

        # Gestion des controles clavier
        if self.get('allow_shortcut', True):
            if isinstance(event, CopyEvent):
                pygame.scrap.put(pygame.SCRAP_TEXT, self.get_selected_text())

            if isinstance(event, CutEvent):
                pygame.scrap.put(pygame.SCRAP_TEXT, self.get_selected_text())
                self.remove_selected_text()

            if isinstance(event, PasteEvent):
                text = pygame.scrap.get(pygame.SCRAP_TEXT)
                self.append_text(text)

            if isinstance(event, SelectAllEvent):
                # On place le curseur en première position
                self.cursor_pos = 0
                # On place le curseur de sélection en dernière position
                self.select_cursor_pos = len(self['text'])
                # On active la sélection
                self.selection = True

            if isinstance(event, FirstDownEvent):
                self.cursor_pos = 0

            if isinstance(event, EndDownEvent):
                self.cursor_pos = len(self['text'])

        if self.get('clickable', True):

            # En cas de clic gauche on place le curseur à l'endroit du clic
            if isinstance(event, MouseLeftDownEvent):
                # On récupère la position du text field
                pos = self.get_actual_position()
                # On calcule la position du text à l'intérieur de celui-ci
                pos = pos[0] + self.text_offset[0], pos[1] + self.text_offset[1]
                relative_cursor_pos = event.pos[0] - pos[0], event.pos[1] - pos[1]
                # On calcul la largeur moyen d'un charactère
                char_av_width = self.surface_size[0]//len(self['text'])

                # On calcule la position du curseur
                compute_cursor_pos = round(relative_cursor_pos[0]/char_av_width)

                # On évite que la position dépasse des bornes
                if compute_cursor_pos > len(self['text']):
                    compute_cursor_pos = len(self['text'])
                elif compute_cursor_pos < 0:
                    compute_cursor_pos = 0
                self.cursor_pos = compute_cursor_pos

            # En cas de double clic on sélectionne tout ou si c'est déjà sélectionner on inverse
            if isinstance(event, MouseDoubleClickEvent):
                if self.selection:
                    self.selection = False
                else:
                    # On place le curseur en première position
                    self.cursor_pos = 0
                    # On place le curseur de sélection en dernière position
                    self.select_cursor_pos = len(self['text'])
                    # On active la sélection
                    self.selection = True

        if isinstance(event, BackspaceDownEvent):
            if self.selection:
                self.remove_selected_text()
            else:
                self.remove_text(LEFT)

        if isinstance(event, DeleteDownEvent):
            if self.selection:
                self.remove_selected_text()
            else:
                self.remove_text(RIGHT)

        if isinstance(event, KeyDownEvent):
            # On arrete le timer le temps d'écrire
            # On le reset pour bien avoir le curseur affiché
            if not event.is_shortcut():
                self.cursor_timer.reset(paused=True)
                char = event.get_char()
                self.append_text(char)

        if isinstance(event, ArrowDownEvent):
            # On reset/arrete le timer le temps de déplacé le curseur
            self.cursor_timer.reset(paused=True)

            # Si Maj est enfoncé on applique la sélection
            if event.modifiers & pygame.KMOD_RSHIFT or event.modifiers & pygame.KMOD_LSHIFT:
                self.select_text(event.direction)
            else:
            # Sinon on bouge juste le curseur
                self.selection = False
                self.move_cursor(event.direction)

        if isinstance(event, KeyUpEvent):
            # On relance le timer si une touche est relevé
            self.cursor_timer.play()

        # On renvoie l'évènement callback si on reçoit l'évènement "Return"
        if isinstance(event, ReturnDownEvent):
            if 'callback' in self and self['callback']:
                self.ev_manager.post(self['callback'](self))

    def draw(self, surface, offset=(0, 0)):
        # Si la vue n'est pas en mémoire on l'a créer avec flash
        if not self.surface:
            self.flash()

        # On récupère une copie du rectangle de positionnement
        blit_rect = self.rect.copy()
        # On ajuste sa position avec l'offset donné ainsi que l'offset en mémoire
        blit_rect.x += offset[0] + self.offset[0]
        blit_rect.y += offset[1] + self.offset[1]

        # On créer le fond qui recevra le texte
        background_surface = pygame.Surface((self.rect.w, self.rect.h))
        background_surface.fill(self['fill'])

        if self._active and 'fill_active' in self:
            background_surface.fill(self['fill_active'])

        # On prépare le curseur
        cursor_rect = pygame.Rect(self.text_offset[0] + self['font'].size(self['text'][:self.cursor_pos])[0], self.text_offset[1]-2, 2, self.surface.get_height())
        # On vérifie la position du curseur, si il dépasse on déplace le texte via son offset
        if cursor_rect.x > self.rect.w*(1 - TEXTVIEW_MIN_INTERVAL):
            cursor_rect.x = int(self.rect.w * (1 - TEXTVIEW_MIN_INTERVAL))
            self.text_offset = int(self.rect.w*(1 - TEXTVIEW_MIN_INTERVAL) - self['font'].size(self['text'][:self.cursor_pos])[0]), self.text_offset[1]
        elif cursor_rect.x < self.rect.w * TEXTVIEW_MIN_INTERVAL:
            cursor_rect.x = int(self.rect.w * TEXTVIEW_MIN_INTERVAL)
            self.text_offset = int(self.rect.w * TEXTVIEW_MIN_INTERVAL - self['font'].size(self['text'][:self.cursor_pos])[0]), self.text_offset[1]

        # On affiche si besoin la sélection
        if self.selection:
            if self.cursor_pos > self.select_cursor_pos:
                x = self.text_offset[0] + self['font'].size(self['text'][:self.select_cursor_pos])[0]
                w = cursor_rect.x - x
            else:
                x = cursor_rect.x
                w = self.text_offset[0] + self['font'].size(self['text'][:self.select_cursor_pos])[0] - x

            selection_rect = pygame.Rect(x, self.text_offset[1]-2, w, self.surface.get_height())
            selection_surface = pygame.Surface((w, self.surface.get_height()))
            selection_surface.fill(self.get('fillSelection', (100, 100, 255)))

            background_surface.blit(selection_surface, selection_rect)

        # Si le textfield est actif et que l'état du cursor_timer est bien sur True
        if self._active:
            if self.cursor_timer.get_state():
                cursor_surface = pygame.Surface((2, self.surface.get_height()))
                cursor_surface.fill(self.get('cursor_color', (0, 0, 0)))
                # On place le texte dans la surface
                background_surface.blit(self.surface, pygame.Rect(self.text_offset[0], self.text_offset[1], self.surface.get_width(), self.surface.get_height()))
                # On place le curseur dans la surface copiée
                background_surface.blit(cursor_surface, cursor_rect)
            else:
                # On ne fait que placer le texte dans la surface
                background_surface.blit(self.surface, pygame.Rect(self.text_offset[0], self.text_offset[1], self.surface.get_width(), self.surface.get_height()))
            self.request_parent_clear = True
        else:
            # On ne fait que placer le texte dans la surface
            background_surface.blit(self.surface, pygame.Rect(self.text_offset[0], self.text_offset[1], self.surface.get_width(), self.surface.get_height()))


        # On dessine sur la surface donnée
        surface.blit(background_surface, blit_rect)

        # Ce n'est plus le premier dessin
        self.first_draw = False


"""
    Example :

    TextField({
        'name': 'TextField',
        'size': SIZE_ABSOLUTE|SIZE_COEF,
        'width': 1,
        'height': 1,
        'position': POSITION_ABSOLUTE|POSITION_COEF,
        'x': 0,
        'y': 0,
        'font': pygame.font.Font(None, 30),
        'text': 'Text',
        'text_align': TEXT_CENTER|TEXT_LEFT|TEXT_RIGHT,
        'text_color': (0, 0, 0),
        'text_color_active': (255, 255, 255),
        'fill': (255, 255, 255),
        'fill_active': (255, 0, 0),
        'cursor_color': (0, 0, 0),
        'callback': CacaoEvent,
        'keep_active': False,
        'clickable': True
    }, ev_manager)
"""

# EVENTS

class TextFieldSubmitEvent(Event):

    def __init__(self, sender):
        super().__init__(sender)
        self.name = "Text Field Submit Event"