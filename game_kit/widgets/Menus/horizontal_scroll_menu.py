from .menu import *
from ...controllers.device_controllers import *
from ...constants import *
from ...utils import *


class HorizontalScrollMenu(Menu):

    def __init__(self, structure, container):
        super().__init__(structure, container)

        # Propriété pour le scroll
        self.offset_x = 0
        self.cur_item_x = 0
        self.cur_item_width = 0
        self.view_width = 0
        self.item_average_width = 0
        self.item_average_visible = 0

        self.loop_timer = Timer({
            'delay': 200,
            'loop': False,
            'state': [
                True,
                False
            ]
        }, paused=True)

    def update(self, dt):

        self.loop_timer.update(dt)
        # On calcule un offset pour que l'item se trouve bien dans l'écran
        if self.cur_item_x < (1/6)*self.view_width:
            self.offset_x += 100*dt + ((1/6)*self.view_width - self.cur_item_x) / 10
        # On ajoute la largeur de l'item pour avoir son côté droit à la limite
        if self.cur_item_x + self.cur_item_width > (1 - 1/6)*self.view_width:
            self.offset_x -= 100*dt + (self.cur_item_x + self.cur_item_width - (1 - 1/6)*self.view_width) / 10

    # On réécris la méthode de dessin car le menu n'a pas de vue propre
    def draw(self, surface, offset=(0, 0)):
        # On récupère la hauteur de la surface sur laquelle on dessine
        self.view_width = surface.get_width()
        # On calcule la largeur du menu
        self.rect.w = 0
        for item in self['items']:
            # On donne en argument les dimensions du menu pour les calculs utilisant des coefficients
            item.compute_size(self.rect)
            self.rect.w += item.rect.w + self['interval']

        self.item_average_width = self.rect.w / len(self['items'])

        self.item_average_visible = self.view_width / self.item_average_width + 2

        # On initialise l'offset du menu
        if self.first_draw:
            self.offset_x = self.view_width//6

        # On ajoute à la hauteur actuelle l'offset calculé pour que l'item sélectionner soit bien dans l'écran
        # print(self.rect.y)
        self.rect.x += self.offset_x

        horizontal_offset = 0
        for index, item in enumerate(self['items']):
            # transmission des arguments
            item.index = index
            # Récupération de l'état de l'item
            is_selected = True if self.cur_item_id == index else False
            # si l'état est différent on met à jour
            if item.selected != is_selected:
                item.selected = is_selected
                # On force l'item à se redessiner
                item.clear()
            # Calcul de l'écart de distance de l'item par rapport à son origine
            offset = self.rect.x + horizontal_offset, self.rect.y
            # Si l'item est sélectionné
            if item.selected:
                # On récupère sa position et sa largeur
                self.cur_item_x = offset[0]
                self.cur_item_width = item.rect.w
            # On vérifie si l'item est visible OU si le timer de boucle tourne
            if self.is_visible(index) or not self.loop_timer.paused:
                # On dessine l'item sur la surface
                item.draw(surface, offset)

            # On ajoute à l'offset un interval ainsi que la hauteur de l'item
            horizontal_offset += self['interval'] + item.rect.w

        # Ce n'est plus le premier dessin
        self.first_draw = False

    # On regarde si l'item est potentiellement visible
    def is_visible(self, index):
        if self.cur_item_id - self.item_average_visible < index < self.cur_item_id + self.item_average_visible:
            return True
        return False

    def select_next_item(self):
        index = self.cur_item_id
        index += 1
        if index >= len(self['items']):
            if self['loop']:
                index = 0
                self.loop_timer['delay'] = 20 * len(self['items'])
                self.loop_timer.reset(paused=False)
            else:
                index -= 1
        self.cur_item_id = index

    def select_previous_item(self):
        index = self.cur_item_id
        index -= 1
        if index < 0:
            if self['loop']:
                index = len(self['items']) - 1
                self.loop_timer['delay'] = 20 * len(self['items'])
                self.loop_timer.reset(paused=False)
            else:
                index += 1
        self.cur_item_id = index

    def handle(self, event):
        #On appelle la méthode originale
        super().handle(event)

        if isinstance(event, ArrowDownEvent):
            if event.direction == LEFT:
                self.select_previous_item()
            elif event.direction == RIGHT:
                self.select_next_item()


"""
    Example :

    HorizontalScrollMenu({
            'name': 'Welcome Menu',
            'size': SIZE_COEF,
            'width': 0,
            'height': 100,
            'position': POSITION_COEF,
            'x': 0,
            'y': 1/4,
            'interval': 25,
            'loop': True,
            'items': [
                MenuItem({
                    'name': 'item 1',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'callback': CacaoEvent,
                    'children': [
                        Label({
                            'name': 'Map 1',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 1',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]

                }, self.ev_manager),
                MenuItem({
                    'name': 'item 2',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'callback': CacaoEvent,
                    'children': [
                        Label({
                            'name': 'Item 2',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Map 2',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]

                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': 300,
                    'height': 300,
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager),
                MenuItem({
                    'name': 'item 3',
                    'size': SIZE_ABSOLUTE,
                    'width': itemSize[0],
                    'height': itemSize[1],
                    'fill': (255, 255, 255),
                    'fill_selected': (255, 0, 0),
                    'children': [
                        Label({
                            'name': 'Item 3',
                            'size': SIZE_COEF,
                            'width': 1,
                            'height': 1,
                            'position': POSITION_ABSOLUTE,
                            'x': 0,
                            'y': 0,
                            'font': pygame.font.Font(None, 30),
                            'text': 'Item 3',
                            'text_align': TEXT_CENTER,
                            'text_color': (0, 0, 0),
                            'text_color_active': (255, 255, 255)
                        }, self.ev_manager)
                    ]
                }, self.ev_manager)
            ]
        }, self.ev_manager)
"""