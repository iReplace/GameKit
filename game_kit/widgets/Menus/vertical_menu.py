from .menu import *
from ...controllers.device_controllers import *
from ...constants import *


class VerticalMenu(Menu):

    def __init__(self, structure, container):
        super().__init__(structure, container)

    # On réécris la méthode de dessin car le menu n'a pas de vue propre
    def draw(self, surface, offset=(0, 0)):
        # On calcule la hauteur du menu pour pouvoir le centrer
        self.rect.h = 0
        for item in self['items']:
            # On donne en argument les dimensions du menu pour les calculs utilisant des coefficients
            item.compute_size(self.rect)
            self.rect.h += item.rect.h + self['interval']

        # On centre le menu sur l'axe vertical
        self.rect.y += surface.get_height()/2 - self.rect.h/2

        vertical_offset = 0
        for index, item in enumerate(self['items']):
            # transmission des arguments
            item.index = index
            # Récupération de l'état de l'item
            is_selected = True if self.cur_item_id == index else False
            # si l'état est différent on met à jour
            if item.selected != is_selected:
                item.selected = is_selected
                # On force l'item à se redessiner
                item.clear()
            # Calcul de l'écart de distance de l'item par rapport à son origine
            offset = self.rect.x, self.rect.y + vertical_offset
            # On dessine l'item sur la surface
            item.draw(surface, offset)
            # On ajoute à l'offset un interval ainsi que la hauteur de l'item
            vertical_offset += self['interval'] + item.rect.h

        # Ce n'est plus le premier dessin
        self.first_draw = False

    def select_next_item(self):
        index = self.cur_item_id
        index += 1
        if index >= len(self['items']):
            if self['loop']:
                index = 0
            else:
                index -= 1
        self.cur_item_id = index

    def select_previous_item(self):
        index = self.cur_item_id
        index -= 1
        if index < 0:
            if self['loop']:
                index = len(self['items']) - 1
            else:
                index += 1
        self.cur_item_id = index

    def handle(self, event):
        #On appelle la méthode originale
        super().handle(event)

        if isinstance(event, ArrowDownEvent):
            if event.direction == UP:
                self.select_previous_item()
            elif event.direction == DOWN:
                self.select_next_item()


"""
    Example :

    VerticalMenu({
        'name': 'VerticalMenu',
        'size': SIZE_ABSOLUTE|SIZE_COEF,
        'width': 1,
        'height': 1,
        'position': POSITION_ABSOLUTE|POSITION_COEF,
        'x': 0,
        'y': 0,
        'interval': 25,
        'loop': True|False,
        'items': []
    }, ev_manager)
"""