from ..widget import *
from ...controllers.device_controllers import *


class MenuItem(Widget):
    def __init__(self, structure, container):
        super().__init__(structure, container)

        # Dictionnaire des surfaces des enfants du menu item
        self.children_surface = []

        # Propriétés
        self.index = 0
        self.selected = False

        self.clear()

    def update(self, dt):
        request_clear = False
        for child in self['children']:
            child.update(dt)
            if child.check_clear_request():
                request_clear = True

        if request_clear:
            self.clear(clear_request=False)


    def flash(self):
        # Création de la surface de l'item
        item_surface = pygame.Surface(self.rect.size)
        item_surface.fill(self['fill'])
        if self.selected:
            item_surface.fill(self['fill_selected'])

        for index, child in enumerate(self['children']):
            # Transmission des valeurs
            child.index = index
            child.set_active(self.selected)  # Le widget enfant est actif seulement si le menu est sélectionné
            # Calcul de la taille/position avec transmission du rect
            child.compute_size(self.rect)
            child.compute_position(self.rect)
            # On dessine l'enfant sur la surface de l'item
            child.draw(item_surface)

        # On place la surface créer en mémoire
        self.surface = item_surface

    def handle(self, event):

        if self.selected:
            if isinstance(event, ReturnDownEvent):
                if 'callback' in self and self['callback']:
                    self.ev_manager.post(self['callback'](self))

            if isinstance(event, MouseDoubleClickEvent):
                if self.is_click(event.pos) and self.get('clickable', True):
                    if 'callback' in self and self['callback']:
                        self.ev_manager.post(self['callback'](self))

"""
    Example :

    MenuItem({
        'name': 'MenuItem',
        'size': SIZE_ABSOLUTE|SIZE_COEF,
        'width': 1,
        'height': 1,
        'position': POSITION_ABSOLUTE|POSITION_COEF,
        'x': 0,
        'y': 0,
        'fill': (255, 255, 255),
        'fill_selected': (255, 0, 0),
        'next_row_target': 0,         # Only with BidimensionalMenu
        'children': [],
        'callback': Event               # Optional
    }, ev_manager)
"""