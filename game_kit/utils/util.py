

class Util:

    def __init__(self, structure):
        self.structure = structure

    # Raccourci d'acces à la structure
    def __getitem__(self, item):
        return self.structure[item]

    # Raccourci de modification de la structure
    def __setitem__(self, key, value):
        self.structure[key] = value

    # Vérification rapide de la présence d'un élément
    def __contains__(self, item):
        if item in self.structure:
            return True
        return False

    # Permet de récupérer un élement avec une valeur par défaut si il n'existe pas
    def get(self, item, default):
        return self.structure.get(item, default)

    def update(self, dt):
        pass

    def handle(self, event):
        pass