import pygame


class ViewManager:

    def __init__(self, container):
        self.container = container
        self.game = self.container.game_controller.entities['game']

        self.window = pygame.display.set_mode(self.game.window_size)
        pygame.display.set_caption(self.game.title)

        self.clear_request_rect = []
        self.font = pygame.font.Font(None, 40)

    def flip(self):
        if len(self.clear_request_rect) > 0:
            pygame.display.update(self.clear_request_rect)
            self.clear_request_rect = []

    def draw_scene(self, scene):
        self.window.fill((0, 0, 0))
        scene.draw(self.window)

        # self.clear_request_rect += scene.check_clear_request_rect()

    def draw_fps(self, fps):
        fps_width, fps_height = (50, 30)
        windowWidth, windowHeight = self.window.get_size()

        # fpsImage
    
        fps_img = pygame.Surface((fps_width, fps_height))
        fps_color = (255, 0, 0)
        fps_img.fill(fps_color)
        fps_rect = pygame.Rect((windowWidth - fps_width, 0, fps_width, fps_height))

        # fpsText
        fps_text_img = self.font.render(fps, 1, (255, 255, 255))
        fps_text_width, fps_text_height = fps_text_img.get_size()
        fps_text_rect = ((fps_width/2 - fps_text_width/2), (fps_height/2 - fps_text_height/2))

        fps_img.blit(fps_text_img, fps_text_rect)

        self.window.blit(fps_img, fps_rect)
        self.clear_request_rect.append(fps_rect)

