
class EventManager:

    def __init__(self):
        from weakref import WeakKeyDictionary
        self.listeners = WeakKeyDictionary()

        self.iterating = False
        self.wait_registration_list = []
        self.wait_unregistration_list = []

    def register(self, listener):
        if self.iterating:
            self.wait_registration_list.append(listener)
        else:
            self.listeners[listener] = 1

    def unregister(self, listener):
        if self.iterating:
            self.wait_unregistration_list.append(listener)
        else:
            if listener in self.listeners.keys():
                del self.listeners[listener]


    def post(self, event):

        for listener in self.wait_registration_list:
            self.register(listener)
        self.wait_registration_list.clear()

        for listener in self.wait_unregistration_list:
            self.unregister(listener)
        self.wait_unregistration_list.clear()

        self.iterating = True
        for listener in self.listeners.keys():
            listener.handle(event)
        self.iterating = False
