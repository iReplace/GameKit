import pygame
from ...constants import *
from ...event import *
from ...utils import *

class MouseController:

    def __init__(self, container):
        self.container = container
        self.ev_manager = self.container.ev_manager

        self.left_click_chronometer = Chronometer({}, paused=False)

    # Permet de mettre à jour le controller en transmettant le dt
    def update(self, dt):
        self.left_click_chronometer.update(dt)
        pass

    # Permet de réagir aux évènement émis par le jeu
    def handle(self, event):
        pass

    # Permet de réagir aux évènements émis par pygame qui sert ici d'interface avec le matériel
    def handle_pygame_event(self, event):
        # Un bouton est enfoncé
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                # Si le dernier click à eu lieu il y'a moins de 50ms on considère que c'est un double clic
                if self.left_click_chronometer.time <= 0.2:
                    self.ev_manager.post(MouseDoubleClickEvent(self, event))
                else:
                    self.ev_manager.post(MouseLeftDownEvent(self, event))

                # On reset le chronomètre
                self.left_click_chronometer.reset(paused=False)

        # Un bouton est relaché
        if event.type == pygame.MOUSEBUTTONUP:
            pass
        # La souris est déplacé
        if event.type == pygame.MOUSEMOTION:
            pass


# EVENTS
# On retrouve ici tous les évènements associés à la souris

class MouseEvent(Event):
    def __init__(self, sender, pygame_event):
        super().__init__(sender)
        self.name = "Mouse Event"
        self.pygame_event = pygame_event
        self.pos = self.pygame_event.pos


class MouseDoubleClickEvent(MouseEvent):
    def __init__(self, sender, pygame_event):
        super().__init__(sender, pygame_event)
        self.name = "Mouse Double Click Event"

class MouseLeftDownEvent(MouseEvent):
    def __init__(self, sender, pygame_event):
        super().__init__(sender, pygame_event)
        self.name = "Mouse Left Down Event"


class MouseRightDownEvent(MouseEvent):
    def __init__(self, sender, pygame_event):
        super().__init__(sender, pygame_event)
        self.name = "Mouse Right Down Event"


class MouseScrollWheelDownEvent(MouseEvent):
    def __init__(self, sender, pygame_event):
        super().__init__(sender, pygame_event)
        self.name = "Mouse Scroll Wheel Down Event"


class MouseLeftUpEvent(MouseEvent):
    def __init__(self, sender, pygame_event):
        super().__init__(sender, pygame_event)
        self.name = "Mouse Left Up Event"


class MouseRightUpEvent(MouseEvent):
    def __init__(self, sender, pygame_event):
        super().__init__(sender, pygame_event)
        self.name = "Mouse Right Up Event"


class MouseScrollWheelUpEvent(MouseEvent):
    def __init__(self, sender, pygame_event):
        super().__init__(sender, pygame_event)
        self.name = "Mouse Scroll Wheel Up Event"


class MouseMotionEvent(MouseEvent):
    def __init__(self, sender, pygame_event):
        super().__init__(sender, pygame_event)
        self.name = "Mouse Motion Event"
