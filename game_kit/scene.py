import pygame


class Scene:

    def __init__(self, container):
        self.container = container
        self.ev_manager = self.container.ev_manager

        self.ev_manager.register(self)

        # Création du dictionnaire contenant les widgets
        self.widgets = {}

        self.rect = None
        self.surface = None

        self.clear_request_rect = []

        self.background_color = (0, 0, 0)

    def __del__(self):
        self.ev_manager.unregister(self)

    def register(self, widget, layer=1):
        self.widgets[widget] = layer

    def unregister(self, widget):
        if widget in self.widgets.keys():
            del self.widgets[widget]

    def update(self, dt):
        # On transmet l'update à tous les widgets
        # for widget in self.widgets.keys():
            # widget.update(dt)
        pass

    def flash(self):
        self.surface = pygame.Surface(self.rect.size)
        self.surface.fill(self.background_color)

        # On transmet la vue à chaque widget pour qu'il puisse se dessiner
        # On utilise sorted pour dessiner les widgets par layer
        for widget in sorted(self.widgets, key=self.widgets.get):
            widget.compute_size(self.rect)
            widget.compute_position(self.rect)
            widget.draw(self.surface)

        self.clear_request_rect.append(self.rect)

    def draw(self, surface):

        if not self.rect:
            self.rect = pygame.Rect((0, 0), surface.get_size())
        if not self.surface:
            self.flash()

        for widget in sorted(self.widgets, key=self.widgets.get):
            if widget.check_clear_request():
                widget.compute_size(self.rect)
                widget.compute_position(self.rect)
                widget.draw(self.surface)
                self.clear_request_rect.append(widget.rect)

        # On dessiner la surface sur celle donnée
        surface.blit(self.surface, self.rect)

    def clear(self):
        self.surface = None

    def handle(self, event):
        pass

    def check_clear_request_rect(self):
        request_clear_rect = self.clear_request_rect.copy()
        self.clear_request_rect = []

        return request_clear_rect