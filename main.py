import pygame
import game_kit as gk
import gk_test

def main():

    game_controller = gk.GameController()

    game_class = gk_test.GkTestGame
    game_controller.add_entity('game', game_class)

    # On transet la scène à loader en premier
    game_controller.preload_scene(gk_test.TextFieldScene)
    game_controller.run()

if __name__ == "__main__":
    main()
